import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:getwidget/getwidget.dart';

class TimePick extends StatefulWidget {
  const TimePick({Key? key}) : super(key: key);

  @override
  _PickState createState() => _PickState();
}

class _PickState extends State<TimePick> {
  // TextEditingController _emailController = TextEditingController();
  // TextEditingController _passwordController = TextEditingController();

  // final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff3D7F7B),
      ),
      body: ListView(
        padding: EdgeInsets.all(10),
        children: <Widget>[
          Center(
            child: Column(
              children: [
                Card(
                  margin: const EdgeInsets.only(top: 20, bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '07.00 - 08.00',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 71,
                          height: 35,
                          child: ElevatedButton(
                            // textColor: Colors.white,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: Color(0xff3D7F7B),
                            ),
                            child: Text(
                              "Pilih",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/submit');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  margin: const EdgeInsets.only(bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '08.00 - 09.00',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 71,
                          height: 35,
                          child: ElevatedButton(
                            // textColor: Colors.white,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: Color(0xff3D7F7B),
                            ),
                            child: Text(
                              "Pilih",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/submit');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  margin: const EdgeInsets.only(bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '09.00 - 10.00',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 71,
                          height: 35,
                          child: ElevatedButton(
                            // textColor: Colors.white,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: Color(0xff3D7F7B),
                            ),
                            child: Text(
                              "Pilih",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/submit');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  margin: const EdgeInsets.only(bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '10.00 - 11.00',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 71,
                          height: 35,
                          child: ElevatedButton(
                            // textColor: Colors.white,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: Color(0xff3D7F7B),
                            ),
                            child: Text(
                              "Pilih",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/submit');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  margin: const EdgeInsets.only(bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '11.00 - 12.00',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 71,
                          height: 35,
                          child: ElevatedButton(
                            // textColor: Colors.white,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: Color(0xff3D7F7B),
                            ),
                            child: Text(
                              "Pilih",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/submit');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  margin: const EdgeInsets.only(bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '12.00 - 13.00',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 71,
                          height: 35,
                          child: ElevatedButton(
                            // textColor: Colors.white,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: Color(0xff3D7F7B),
                            ),
                            child: Text(
                              "Pilih",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/submit');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Card(
                  margin: const EdgeInsets.only(bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          '13.00 - 14.00',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 71,
                          height: 35,
                          child: ElevatedButton(
                            // textColor: Colors.white,
                            style: ElevatedButton.styleFrom(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                              ),
                              backgroundColor: Color(0xff3D7F7B),
                            ),
                            child: Text(
                              "Pilih",
                              style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w700,
                                fontSize: 16,
                              ),
                            ),
                            onPressed: () {
                              Navigator.pushNamed(context, '/submit');
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
