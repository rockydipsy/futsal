import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({super.key});

  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<AccountScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("Login Screen App"),
      // ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 54),
              width: 159.0,
              height: 159.0,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage("assets/icon/heroicons_user.png"),
                ),
                border: Border.all(
                  color: Color(0xff3D3E3B),
                  width: 1.0,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(10),
              margin: const EdgeInsets.only(top: 17),
              child: Text(
                "Nama Lengkap",
                style: GoogleFonts.poppins(
                  color: Color(0xff9C9C9C),
                  fontWeight: FontWeight.w500,
                  fontSize: 19,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 30),
              width: 333,
              height: 48,
              child: ElevatedButton(
                // textColor: Colors.white,
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                    backgroundColor: Colors.red),
                child: Text(
                  "Log Out",
                  style: GoogleFonts.roboto(
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamed(context, '/main-home');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
