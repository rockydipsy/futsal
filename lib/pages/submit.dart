import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Submit extends StatefulWidget {
  const Submit({Key? key}) : super(key: key);

  @override
  _SubmitState createState() => _SubmitState();
}

class _SubmitState extends State<Submit> {
  // TextEditingController _emailController = TextEditingController();
  // TextEditingController _passwordController = TextEditingController();

  // final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff3D7F7B),
      ),
      body: ListView(
        padding: EdgeInsets.all(10),
        children: <Widget>[
          Center(
            child: Column(
              children: [
                Card(
                  margin: const EdgeInsets.only(top: 20, bottom: 13),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      ListTile(
                        title: Text(
                          'Nama : ',
                          style: GoogleFonts.redHatDisplay(
                            color: Color(0xff120E21),
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        trailing: Container(
                          width: 220,
                          height: 30,
                          child: TextFormField(
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(0),
                              ),
                              filled: true,
                              fillColor: Color(0xffD9D9D9),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 30, top: 400),
                  width: 342,
                  height: 50,
                  child: ElevatedButton(
                    // textColor: Colors.white,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      backgroundColor: Color(0xff3D7F7B),
                    ),
                    child: Text(
                      "Submit",
                      style: GoogleFonts.roboto(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/');
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
