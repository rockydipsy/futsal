import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:getwidget/getwidget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  // TextEditingController _emailController = TextEditingController();
  // TextEditingController _passwordController = TextEditingController();

  // final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.all(10),
        children: <Widget>[
          Center(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 37, top: 59),
                  width: 342,
                  height: 55,
                  child: TextFormField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      filled: true,
                      fillColor: Color(0xffE6EAEB),
                      labelText: "PILIH WAKTU",
                      labelStyle: GoogleFonts.poppins(
                        fontSize: 14,
                        color: Color(0xff9F9F9F),
                        fontWeight: FontWeight.w400,
                      ),
                      suffixIcon: const Icon(
                        Icons.calendar_month_outlined,
                        color: Color(0xff383838),
                        size: 35,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 30),
                  width: 342,
                  height: 50,
                  child: ElevatedButton(
                    // textColor: Colors.white,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      backgroundColor: Color(0xff3D7F7B),
                    ),
                    child: Text(
                      "Submit",
                      style: GoogleFonts.roboto(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      ),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, '/pick');
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(10),
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "News",
                    style: GoogleFonts.poppins(
                      color: Color(0xff000000),
                      fontWeight: FontWeight.w500,
                      fontSize: 15,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 20.0),
                  height: 200.0,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(right: 23),
                        width: 230,
                        height: 199,
                        child: Card(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.center,
                                child: Image.asset('assets/img/gambar2.png'),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 9),
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(right: 60, left: 10),
                                      child: Text(
                                        "pertandingan",
                                        style: GoogleFonts.inter(
                                          color: Color(0xff383838),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.check,
                                      color: Color(0xff538DCD),
                                      size: 15,
                                    ),
                                    Text(
                                      "Tersedia",
                                      style: GoogleFonts.poppins(
                                        color: Color(0xff538DCD),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 9),
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(right: 100, left: 10),
                                      child: Text(
                                        "Within 500m",
                                        style: GoogleFonts.poppins(
                                          color: Color(0xff8B8B8B),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "4.5",
                                      style: GoogleFonts.inter(
                                        color: Color(0xff383838),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 11,
                                      ),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Color(0xffFFCC00),
                                      size: 15,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        width: 230,
                        height: 199,
                        child: Card(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                alignment: Alignment.center,
                                child: Image.asset('assets/img/gambar2.png'),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 9),
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(right: 85, left: 10),
                                      child: Text(
                                        "IFI Futsal",
                                        style: GoogleFonts.inter(
                                          color: Color(0xff383838),
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12,
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.check,
                                      color: Color(0xff538DCD),
                                      size: 15,
                                    ),
                                    Text(
                                      "Tersedia",
                                      style: GoogleFonts.poppins(
                                        color: Color(0xff538DCD),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.only(top: 9),
                                child: Row(
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.only(right: 100, left: 10),
                                      child: Text(
                                        "Within 500m",
                                        style: GoogleFonts.poppins(
                                          color: Color(0xff8B8B8B),
                                          fontWeight: FontWeight.w400,
                                          fontSize: 10,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "4.5",
                                      style: GoogleFonts.inter(
                                        color: Color(0xff383838),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 11,
                                      ),
                                    ),
                                    Icon(
                                      Icons.star,
                                      color: Color(0xffFFCC00),
                                      size: 15,
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
