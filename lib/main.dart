import 'package:flutter/material.dart';
import 'package:project_futsal/main_home.dart';
import 'package:project_futsal/pages/home.dart';
import 'package:project_futsal/main_home.dart';
import 'package:project_futsal/pages/time_pick.dart';
import 'package:project_futsal/pages/submit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Futsal',
      routes: {
        '/': (context) => MainHomeScreen(),
        '/pick': (context) => TimePick(),
        '/submit': (context) => Submit(),
      },
    );
  }
}
